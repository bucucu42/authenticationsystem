<?php

// start session
session_start();

// Destroy user session
unset($_SESSION['user_id']);

// Redirect to login.php page
header("Location: login.php");
