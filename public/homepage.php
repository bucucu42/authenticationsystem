<?php

// Start Session
session_start();

// check user login
if(empty($_SESSION['user_id']))
{
    header("Location: login.php");
}

// Database connection
require __DIR__ . '/../private/db_con.php';
$db = DB();

// Functions
require __DIR__ . '/../private/functions.php';
$app = new data_to_database($db);
$user = $app->UserDetails($_SESSION['user_id']);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Homepage</title>
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <header>
        <div class="menu">
            <span class="bar"></span>
        </div>
        <nav>
            <div class="nav-container">

            </div>
            <ul>
                <li><a href="homepage.php" class='active-page'>Profile</a></li>
                <li><a href="logout.php">Logout</a></li>
            </ul>
        </nav>
        <div class="main-text">

            <h1 class="profile">Welcome <?= htmlspecialchars($user->name) ?> </h1>
            <h2> Account Details:</h2>
            <!-- HTML ESCAPING-->
            <p>Name: <?= htmlspecialchars($user->name) ?> </p>
            <p>Username: <?= htmlspecialchars($user->username) ?> </p>
            <p>Email: <?= htmlspecialchars($user->email) ?> </p>

        </div>
    </header>


<script type="text/javascript">
    // Check for inactivity
    window.onload = function() {
        inactivityTime();
    }

    var inactivityTime = function () {
        var time;
        window.onload = resetTimer;
        // DOM Events
        document.onmousemove = resetTimer;
        document.onkeydown = resetTimer;
        document.onclick = resetTimer;

        function logout() {
            alert("Your session has expired due to inactivity. Please login again.")
            location.href = 'logout.php'
        }

        function resetTimer() {
            clearTimeout(time);
            time = setTimeout(logout, 900000) //Timeout for 15 Min

        }
    };


</script>


</body>

</html>