<?php

// Start Session
session_start();

// Database connection
require __DIR__ . '/../private/db_con.php';
$db = DB();

// Functions
require __DIR__ . '/../private/functions.php';
$app = new data_to_database($db);

require_once __DIR__ . '/../private/Authenticator.php';
$pga = new Authenticator();
$secret = $pga->createSecret();

$register_error_message = '';

// check Register request
if (!empty($_POST['btnRegister']) && isset($_SESSION['token']) && isset($_POST['token']) && $_SESSION['token'] ==  $_POST['token']) {
    if ($_POST['name'] == "") {
        $register_error_message = 'Name field is required!';
    } else if ($_POST['email'] == "") {
        $register_error_message = 'Email field is required!';
    } else if ($_POST['username'] == "") {
        $register_error_message = 'Username field is required!';
    } else if ($_POST['password'] == "") {
        $register_error_message = 'Password field is required!';
    } else if(strlen($_POST['password']) < 8 ) {
        $register_error_message .= "Password too short!";
    } else if(strlen($_POST['password']) > 64 ) {
        $register_error_message .= "Password too long!";
    } else if(!preg_match("#[0-9]+#", $_POST['password']) ) {
        $register_error_message .= "Password must include at least one number!";
    } else if( !preg_match("#[a-z]+#", $_POST['password']) ) {
        $register_error_message .= "Password must include at least one lower case letter!";
    } else if( !preg_match("#[A-Z]+#", $_POST['password']) ) {
        $register_error_message .= "Password must include at least one upper case letter!";
    } else if( !preg_match("#\W+#", $_POST['password']) ) {
        $register_error_message .= "Password must include at least one special character (~?!@#$%^&*)!";
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $register_error_message = 'Invalid email address!';
    } else if ($app->isEmail($_POST['email'])) {
        $register_error_message = 'Email is already in use!';
    } else if ($app->isUsername($_POST['username'])) {
        $register_error_message = 'Username is already in use!';
    } else {
        $user_id = $app->Register($_POST['name'], $_POST['email'], $_POST['username'], $_POST['password'], $secret);
        // set session and redirect user to the profile page
        $_SESSION['user_id'] = $user_id;
        header("Location: validate_register.php");
    }
}
$_SESSION['token'] = get_random_string(60); // generate Token
?>


<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/1c2c2462bf.js"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <link rel ="stylesheet" href="../style.css">

    <meta charset="UTF-8">
    <title>Registration</title>

</head>
<body>

<div class="container">

            <form action="registration.php" method="post" class="form-horizontal" id="validateForm">
                <h1>Sign Up</h1>
                <?php
                if ($register_error_message != "") {
                    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $register_error_message . '</div>';
                }
                ?>
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" class="form-control" autocomplete="off"/>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" class="form-control" autocomplete="off"/>
                </div>
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" class="form-control" autocomplete="off"/>
                </div>
                <div class="form-group">
                    <label for="">Password</label>

                    <div class="row">
                        <div class="col-md-12">

                    <input type="password" id="password" name="password" class="form-control input-md" maxlength="64"/>
                    <span class="show-pass" onclick="toggle()">
                        <i class="far fa-eye" onclick="myFunction(this)"></i>
                    </span>
                </div>
                    </div>
                </div>


                <div id="popover-password">
                    <p><span id="result"></span></p>
                    <div class="progress">
                        <div id="password-strength" class="progress-bar"
                             role="progressbar" aria-valuenow="40"
                             aria-valuemin="0" aria-valuemax="100"
                             style="width: 0%"></div>
                    </div>

                </div>

                <ul class="list-unstyled">
                    <li>
                            <span class="low-upper-case">
                                <i class="fas fa-circle" aria-hidden="true">
                                </i> Lowercase &amp; Uppercase
                            </span>
                    </li>
                    <li>
                            <span class="one-number">
                                <i class="fas fa-circle" aria-hidden="true">
                                </i>  Number(0-9)
                            </span>
                    </li>
                    <li>
                            <span class="one-special-char">
                                <i class="fas fa-circle" aria-hidden="true">
                                </i>  Special character (~?!@#$%^&*)
                            </span>
                    </li>
                    <li>
                            <span class="eight-character">
                                <i class="fas fa-circle" aria-hidden="true">
                                </i>  Atleast 8 character
                            </span>
                    </li>
                </ul>

                <div class="form-group">
                    <input type="hidden" name="token" value="<?=$_SESSION['token']?>">
                    <input type="submit" name="btnRegister" class="btn btn-primary" value="Sign Up"/>
                </div>
                <div class="form-group text-center">
                    Already have an account? <a href="login.php">Log In!</a>
                </div>
            </form>
</div>
<script src="../main.js"></script>
</body>
</html>


