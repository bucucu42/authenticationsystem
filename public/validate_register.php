<?php

// Start Session
session_start();



// Database connection
require __DIR__ . '/../private/db_con.php';
$db = DB();


require __DIR__ . '/../private/functions.php';
$app = new data_to_database($db);
$user = $app->UserDetails($_SESSION['user_id']);

require_once __DIR__ . '/../private/Authenticator.php';
$pga = new Authenticator();
$qr_code =  $pga->getQRCodeGoogleUrl($user->email, $user->google_secret_code, 'SecureLogin 2FA');

$error_message = '';

if (isset($_POST['btnValidate']) && isset($_SESSION['token']) && isset($_POST['token']) && $_SESSION['token'] ==  $_POST['token']) {

    $code = $_POST['code'];

    if ($code == "") {
        $error_message = 'Please Scan above QR code to configure your application and enter genereated authentication code to validate!';
    }
    else
    {
        if($pga->verifyCode($user->google_secret_code, $code, 2))
        {
            // success
            header("Location: homepage.php");
        }
        else
        {
            // fail
            $error_message = 'Invalid Authentication Code!';
        }
    }
}

$_SESSION['token'] = get_random_string(60);

?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/1c2c2462bf.js"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <link rel ="stylesheet" href="../style.css">

    <meta charset="UTF-8">
    <title>Two-Factor</title>
</head>
<body>
<div class="container">

            <form method="post" action="validate_register.php" class="form-qr" id="validateForm" >
                <div class="twofact">
                <h1>Two-Factor Authentication </h1>
                </div>
                <?php
                if ($error_message != "") {
                    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $error_message . '</div>';
                }
                ?>
                <p>
                    1. Download and install any authenticator app (Eg. Google Authenticator) on your phone.
                </p>

                <p>
                    2. Scan the QR code below:
                </p>

                <div class="form-group text-center">
                    <img src="<?php echo $qr_code; ?>" >

                </div>

                <div class="form-group text-center">

                    <p>Can't scan QR Code?</p>
                    <p>Alternatively, you can enter the following key into your authenticator app.</p>
                    <div style="border: 1px solid black; border-radius: 5px; background-color:#F5F5F5;">
                    <p>Account: <?php echo $user->email; ?></p>
                    <p>Key: <?php echo $user->google_secret_code; ?></p>
                    <p>Time based: yes</p>
                    </div>

                </div>


                <div class="form-group">
                    <p>3. Enter Verification Code:</p>
                    <input type="text" name="code" placeholder="6-Digit Code" class="form-control" autocomplete="off">
                </div>
                <div class="form-group">
                    <input type="hidden" name="token" value="<?=$_SESSION['token']?>">
                    <button type="submit" name="btnValidate" class="btn btn-primary">Verify</button>
                </div>
                <div class="form-group text-center">
                    Already have an account? <a href="login.php">Log In!</a>
                </div>
            </form>

</div>

</body>
</html>