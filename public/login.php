<?php

// Start Session
session_start();

// Database connection
require __DIR__ . '/../private/db_con.php';
$db = DB();

// Function
require __DIR__ . '/../private/functions.php';
$app = new data_to_database($db);

$login_error_message = '';
$lockout_message ='Please wait for 30 seconds!';


// check Login request and Tokens against CRSF-Protection
if (!empty($_POST['btnLogin']) && isset($_SESSION['token']) && isset($_POST['token']) && $_SESSION['token'] ==  $_POST['token']) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if ($username == "") {
        $login_error_message = 'Username field is required!'; //req field
    } else if ($password == "") {
        $login_error_message = 'Password field is required!'; //req field
    }
    else {
        $_SESSION["login_attempts"] += 1; // add +1 to for Brute-Force-Protection
        $user_id = $app->Login($username, $password); // check user login
        if($user_id > 0)
        {
            $_SESSION['user_id'] = $user_id; // Set Session
            header("Location: validate_login.php"); // Redirect user to validate auth code
        }
        else if ($_SESSION["login_attempts"] > 4) // After 5 Attempts -> Timeout for user
        {
            $login_error_message = 'To many failed login attempts!';

        }

        else{
            $login_error_message = 'Invalid login attempt!';
        }
    }
}

if (isset($_SESSION["locked"]))
{
    $difference = time() - $_SESSION["locked"];
    if ($difference > 30) // Timeout for 30 seconds
    {
        unset($_SESSION["locked"]);
        unset($_SESSION["login_attempts"]);
        $login_error_message = '';

    }
}

$_SESSION['token'] = get_random_string(60); // Generate random String, for Token

?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/1c2c2462bf.js"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600&display=swap" rel="stylesheet">
    <link rel ="stylesheet" href="../style.css">

    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>

<div class="container">

            <form action="login.php" method="post" class="form-horizontal" id="validateForm">
                <h1>Login</h1>
                <?php
                if ($login_error_message != "") {
                    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
                }
                ?>
                <div class="form-group">
                    <label for="">Username/Email</label>

                    <input type="text" name="username" class="form-control" autocomplete="off"/>
                </div>
                <div class="form-group">


                    <label for="">Password</label>


                    <div class="row">
                        <div class="col-md-12">

                    <input type="password" id="password" name="password" class="form-control input-md" maxlength="64"/>


                    <span class="show-pass" onclick="toggle()">
                    <i class="far fa-eye" onclick="myFunction(this)"></i>
                    </span>

                        </div>
                    </div>


                </div>

                <div class="form-group">
                    <label for=""></label>
                </div>


                <div class="form-group text-center">
                    <input type="hidden" name="token" value="<?=$_SESSION['token']?>">
                    <?php // disappearance of the login button, after 5 login attempts
                    if ($_SESSION["login_attempts"] > 4)
                    {
                        $_SESSION["locked"] = time();
                        echo $lockout_message;
                    }
                    else
                    {
                    ?>
                    <input type="submit" name="btnLogin" class="btn btn-primary " value="Login"/>
                    <?php } ?>
                </div>

            <div class="form-group text-center">
                Don't have an account? <a href="registration.php">Sign Up!</a>
            </div>
            </form>

</div>
<script src="../main.js"></script>

</body>
</html>